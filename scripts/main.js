'use strict'


var methods = {
    addSubscribers : function(data){
        var self = this
        $.ajax({
            url : '/php/main.php',
            method : 'POST',
            data : data
        })
        .then(function(response){
            var encoded = JSON.parse(response)
            var msg = encoded.message
            var success = encoded.success
            if(success === 1){
                self.createSuccessMsg(msg)
            }else{
                self.createErrMsg([msg])
            }
        })
        .catch(function(error){
            console.log(error);
        })
    },
    validateForm(){

        var name = $("#name").val().trim()
        var email = $("#email").val().trim()
        var errors = [];

        if(! email || ! name){
            errors.push("All fields are required")
        }   
        
        if(! this.isEmail(email) && email){
            errors.push("Invalid Email")
        }

        if(! this.isAlpha(name)){
            errors.push("Invalid Name. Must be letters and spaces only.")
        }

        if(errors.length){
            this.createErrMsg(errors)
        }else{
            this.addSubscribers({ name : name, email : email})
        }
    },
    isEmail(email){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
        return regex.test(email);
    },
    isAlpha(str){
        var regex = /^[a-zA-Z ]*$/g
        return regex.test(str);
    },
    createErrMsg(errs){
        
        var alert_el = $(".alert")
        var title = document.createElement("strong")
        var ul = document.createElement('ul')
      
        title.innerHTML = "Error" + (errs.length > 1? "s" : "")
        
        for(var key in errs){
            var li = document.createElement('li')
            li.innerHTML = errs[key]
            ul.append(li)
        }

        alert_el.html(ul).prepend(title)
      
        this.replaceClass(alert_el, "alert-success", "alert-danger")
    },
    createSuccessMsg(msg){

        var alert_el = $(".alert");
        var title = document.createElement("strong")
        var message = document.createElement("p")

        title.innerHTML = "Success"
        message.innerHTML = msg

        alert_el.html(message).prepend(title)

        this.replaceClass(alert_el, "alert-danger", "alert-success")
    },
    replaceClass(el, oldClass, newClass){
        el.removeClass(oldClass).addClass(newClass).removeClass("hidden");
    }
}


$( "#Form" ).submit(function( event ) {
  event.preventDefault();
  console.log('prevented')
  methods.validateForm();
});


