<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Job Test</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap  CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- Main CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="/styles/main.css" />
    
</head>
<body>
   
    <div class="container-fluid h-100 darken">
        <div class="row h-100 d-flex">
            <div class="col-md-3 mx-auto align-self-center">
                <div class="card">
                    <h5 class="h-title">Subscribe to our Newsletter</h5>
                    <div class="card-body">
                        <form id="Form" novalidate>
                            <div class="form-group with-icon">
                                <input type="Name" id="name" class="form-control" placeholder="Name">
                                <i class="fas fa-user fa-fw input-icon"></i>
                            </div>
                            <div class="form-group with-icon">
                                <input type="Email" id="email" class="form-control" placeholder="Email">
                                <i class="fas fa-envelope fa-fw input-icon"></i>
                            </div>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="submit" class="btn btn-danger">Subscribe</button>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-caret-right"></i>
                                </button>
                            </div>
                        </form>
                        <div class="alert mt-2 hidden">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
    <!-- Bootstap JS  -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- Main JS -->
    <script src="/scripts/main.js"></script>
</body>
</html>