<?php
    include_once('db_connection/connect.php');

    $request = $_POST;

    try{
        $stmt = $conn->prepare("SELECT * FROM subscribers WHERE email = :email");
        $stmt->bindParam(':email', $request['email']);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count > 0){
            die( json_encode( ['success' => 0, 'message' => 'Your email already subscribe.']));
        }
    } catch( PDOException $e){
        die( json_encode( [ 'success' => 0, 'message' => 'Connection Error : ' .$e->getMessage() ] ));
    }

    try{
        $stmt = $conn->prepare("INSERT INTO subscribers (name, email) VALUES (:name, :email)");
        $stmt->execute($request);
    } catch( PDOException $e){
        die( json_encode( [ 'success' => 0, 'message' => 'Connection Error : ' .$e->getMessage() ] ));
    }

    die( json_encode( [ 'success' => 1, 'message' => 'You successfully subscribe!'] ));

?>